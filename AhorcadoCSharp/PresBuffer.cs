﻿using System;
using System.Collections.Generic;

namespace AhorcadoCSharp
{
    /*
     * ===================================
     * = --¬                             =
     * = | o                             =
     * = |´|`  ____ _____                =
     * = |/'\                            =
     * =                                 =
     * =---------------------------------=
     * =   ABCDEFGHIJKLMNÑOPQRSTUVWXYZ   =
     * ===================================
     */
    public class Game
    {
        private const string LETTERS = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
        private string word;
        private string guessings;
        private string punished;
        private int aToMatch;
        private string matches;
        private int lives;
        private bool refresh;

        public Game() {
            this.word = "";
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@AppDomain.CurrentDomain.BaseDirectory + "/dictionary.txt");
                this.word = lines[new Random().Next(0, lines.Length)];
            } catch(Exception e)
            {
                PrintColorized("The dictionary couldn't be found, please, place it on '" + AppDomain.CurrentDomain.BaseDirectory + "'", ConsoleColor.Red);
                Console.ReadKey(true);
                Environment.Exit(0);
            }
            this.guessings = "";
            this.punished = "";
            this.aToMatch = CountLetters();
            this.matches = "";
            this.lives = 7;
            this.refresh = false;
        }

        public void GameLoop() 
        {
            bool isGameOver = false;
            bool hasWon = false;
            while(!isGameOver)
            {
                PrintGame();
                if (!refresh)
                {
                    char key = Char.ToUpper(Console.ReadKey(true).KeyChar);
                    if (Char.IsLetter(key))
                    {
                        if (!guessings.Contains(key.ToString()))
                            guessings += key;
                    }
                }
                if (lives <= 0)
                    isGameOver = true;
                if (matches.Length == aToMatch) {
                    hasWon = true;
                    isGameOver = true;
                }
                Console.Clear();
            }
            Console.WriteLine();
            if (!hasWon)
                PrintColorized("   GAME OVER!\n", ConsoleColor.Red);
            else
                PrintColorized("   YOU WON!\n", ConsoleColor.Green);

            Console.Write("   The correct word was ");
            PrintColorized(word, ConsoleColor.Cyan);
            Console.Write(".\n   You failed ");
            PrintColorized(7 - lives, ConsoleColor.Red);
            Console.Write(" times.\n\n   +--¬\n   |  ");
            PaintBody(1);
            Console.Write("\n   | ");
            PaintBody(2);
            Console.Write("\n   | ");
            PaintBody(3);
        }

        private void PrintColorized(string text, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ResetColor();
        }

        private void PrintColorized(int text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ResetColor();
        }

        private void PrintGame() {
            int totalLength = 10 + ((word.Length > LETTERS.Length) ? word.Length : LETTERS.Length);

            Console.WriteLine("\n   " + GenString('=', totalLength));
            Console.WriteLine("   = +-¬" + GenString(' ', totalLength - 6) + "=");
            Console.Write("   = | ");

            PaintBody(1);

            Console.WriteLine(GenString(' ', totalLength - 6) + "=");
            Console.Write("   = |");

            PaintBody(2);

            Console.WriteLine(GenString(' ', totalLength - 7) + "=");
            Console.Write("   = |");

            PaintBody(3);
            PrintGuessingRegion();

            Console.WriteLine(GenString(' ', totalLength - (10 + (word.Length))) + "=");
            Console.WriteLine("   = ¯¯¯¯¯" + GenString(' ', totalLength - 8) + "=");
            Console.WriteLine("   =" + GenString('-', totalLength - 2) + "=");

            int offset = ((totalLength - LETTERS.Length) - 2);
            
            Console.Write("   =" + GenString(' ', offset/2));
            PrintLetters();
            if (offset % 2 == 0) {
                //spacing is pair
                Console.WriteLine(GenString(' ', offset / 2) + "=");
            } else {
                //spacing is odd
                Console.WriteLine(GenString(' ', (offset/2) + 1) + "=");
            }

            Console.WriteLine("   " + GenString('=', totalLength));

            refresh = !refresh;
        }

        private void PrintLetters()
        {
            foreach (char l in LETTERS)
            {
                int status = 0;
                //STATUS
                //0 - Non Hit
                //1 - Correct
                //2 - Wrong
                foreach (int hL in guessings.ToCharArray())
                {
                    if (Char.ToUpper(l) == hL)
                    {
                        if (word.ToUpper().Contains(Char.ToUpper(l).ToString()))
                            status = 1;
                        else
                            status = 2;
                    }
                }
                switch (status)
                {
                    case 0:
                        PrintColorized(l.ToString(), ConsoleColor.White);
                        break;
                    case 1:
                        PrintColorized(l.ToString(), ConsoleColor.Green);
                        if (!refresh && !matches.Contains(l.ToString().ToUpper()))
                            matches += l;
                        break;
                    case 2:
                        PrintColorized(l.ToString(), ConsoleColor.Red);
                        if (!refresh && !this.punished.Contains(l.ToString()))
                        {
                            this.punished += l.ToString();
                            lives--;
                        }
                        break;
                }
            }
        }

        private void PrintGuessingRegion()
        {
            foreach (string w in word.Split(' '))
            {
                foreach (char l in w)
                {
                    bool letterHit = false;
                    foreach (char hl in guessings.ToCharArray())
                    {
                        if (Char.ToUpper(l) == hl)
                        {
                            PrintColorized(l.ToString(), ConsoleColor.Green);
                            letterHit = true;
                        }
                    }
                    if (!letterHit)
                        PrintColorized("_", ConsoleColor.Cyan);
                }
                Console.Write(" ");
            }
        }

        private void PaintBody(int row)
        {
            switch(row)
            {
                case 1:
                    if (lives < 7)
                        PrintColorized("o", ConsoleColor.DarkYellow);
                    else
                        Console.Write(" ");
                    break;
                case 2:
                    if(lives < 4)
                        PrintColorized("´|`", ConsoleColor.DarkYellow);
                    else if(lives < 5)
                        PrintColorized("´| ", ConsoleColor.DarkYellow);
                    else if(lives < 6)
                        PrintColorized(" | ", ConsoleColor.DarkYellow);
                    else
                        Console.Write("   ");
                    break;
                case 3:
                    if (lives < 1)
                        PrintColorized("/'\\  ", ConsoleColor.DarkYellow);
                    else if (lives < 2)
                        PrintColorized("/'   ", ConsoleColor.DarkYellow);
                    else if (lives < 3)
                        PrintColorized(" '   ", ConsoleColor.DarkYellow);
                    else
                        Console.Write("     ");
                    break;
            }
        }

        private int CountLetters()
        {
            List<char> temp = new List<char>();
            foreach (string w in word.Split(' '))
            {
                foreach (char l in w)
                {
                    if (!temp.Contains(Char.ToUpper(l)))
                        temp.Add(Char.ToUpper(l));
                }
            }
            return temp.Count;
        }

        private string GenString(char c, int l) {
            return new string(c, l);
        }
    }
}
