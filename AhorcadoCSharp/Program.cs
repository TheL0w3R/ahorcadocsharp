﻿using System;

namespace AhorcadoCSharp
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Game g = new Game();
            while(true)
            {
                new Game().GameLoop();
                Console.WriteLine("\n\n\n   Press ENTER to play again, ESC for exit.");
                ConsoleKey key = 0;
                do {
                    key = Console.ReadKey(true).Key;
                    if (key.Equals(ConsoleKey.Escape))
                        Environment.Exit(0);
                } while (!key.Equals(ConsoleKey.Enter));
            }
        }
    }
}
